<?php
$sql =
"
CREATE TABLE IF NOT EXISTS " . tablename('we7_enterprise_news') . " (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '新闻标题',
  `content` mediumtext NOT NULL COMMENT '新闻内容',
  `uniacid` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS " . tablename('we7_enterprise_setting') . " (
  `key` varchar(100) NOT NULL,
  `value` text,
  `uniacid` int(11) NOT NULL,
  KEY `key` (`key`)
) DEFAULT CHARSET=utf8;
";
pdo_query($sql);